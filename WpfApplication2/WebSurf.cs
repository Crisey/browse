﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WpfApplication2
{
    class WebSurf
    {
        private HttpWebRequest p_WebRequest;
        private HttpWebResponse p_WebResponse;
        private WebBrowser p_Browser;

        #region ctor
        public WebSurf(WebBrowser browser)
        {
            p_Browser = browser;
            p_Browser.Navigate("about:blank");
        }


        #endregion ctor

        public void EnterUrl(Uri url)
        {
            p_WebRequest = (HttpWebRequest)WebRequest.Create(url);
            p_WebRequest.UserAgent = @"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
            p_WebResponse = (HttpWebResponse)p_WebRequest.GetResponse();
            p_Browser.Navigate(url);
        }

        public void EnterPremiumUrl(Uri url, string referer)
        {
            p_WebRequest = (HttpWebRequest)WebRequest.Create(url);
            p_WebRequest.Referer = referer;
            p_WebRequest.UserAgent = @"Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36";
            p_WebResponse = (HttpWebResponse)p_WebRequest.GetResponse();
            p_Browser.Navigate(url);
        }
    }
}
